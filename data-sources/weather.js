/**
 * Weather stuff
 */
/* global require,process */
var _ = require('lodash'),
	Bluebird = require('bluebird');

var Forecast = require('forecast.io-bluebird');
var forecast = new Forecast({
	key: process.env.FORECAST_API_KEY,
	timeout: 2500
});

var weatherCache = {};
var dummyWeather = {
	currently: {
		temperature: 17,
		icon: 'partly-cloudy-day'
	}
};

var WeatherUtil = {
	timing: {
		weather: 1200
	},
	events: {
		weather: function(location, config) {
			var position = config && config.weather;
			if(!position) {
				return dummyWeather;
			}
			
			return forecast.fetch(position.latitude, position.longitude, { units: 'ca' })
				.catch(function(){
					return dummyWeather;
				});
		}
	}
};

module.exports = WeatherUtil;