const prequest = require('request-promise'),
	Bluebird = require('bluebird'),
	_ = require('lodash'),
	Queue = require('better-queue'),
	URL = require('url');

const invalidDomains = /\/(google|apple|gmail|hotmail|outlook|telus|shaw|novus|icloud\.com|me\.com|bing|yahoo|aol\.com|msn\.(com|ca|fr)|live\.(com|ca|fr|co\.uk|nl|it|com\.au)|ymail)\./i;

function getLogURL(item) {
	const logURL = URL.parse(item.url);
	logURL.query = item.params;
	return URL.format(logURL);
}

function logRequest(item, result) {
	NexudusUtil._requestLog.push({
		date: new Date(),
		url: getLogURL(item),
		result: result
	});
	if(NexudusUtil._requestLog.length > 300) {
		NexudusUtil._requestLog = NexudusUtil._requestLog.slice((NexudusUtil._requestLog.length - 300));
	}
}

function shuffleArray(array) {
	for (let i = array.length - 1; i > 0; i--) {
		const j = Math.floor(Math.random() * (i + 1));
		[array[i], array[j]] = [array[j], array[i]];
	}
}

const requestQueue = new Queue(function(item, cb) {
	prequest({
		url: item.url,
		'auth': {
			'user': process.env.USER,
			'pass': process.env.PASSWORD,
			'sendImmediately': true
		},
		qs: item.params,
		json: true
	})
	.then(function(result) {
		logRequest(item, 'success');
		return result;
	})
	.catch(function(err) {
		logRequest(item, err.message);
		throw err;
	})
	.nodeify(cb);
}, { afterProcessDelay: 200, maxTimeout: 10000, maxRetries: 3, retryDelay: 10000 });

// To get user image https://spacekraft.spaces.nexudus.com/en/coworker/getavatar/{user id}?h=50&w=50
// To get team image https://spacekraft.spaces.nexudus.com/en/team/getavatar/{user id}?h=50&w=50
// To get resource image https://spacekraft.spaces.nexudus.com/en/publicresources/getimage/{resource id}?h=100&w=200
const NexudusUtil = {
	timing: {
		bookings: 600, // Update rooms every 10 minutes
		events: 3600, // Update events every hour
		businesses: 43200 // Update featured members every 12hr
	},
	events: {
		bookings: function(location, config) {
			return NexudusUtil.getRoomStatus(config.nexudus, config.business, config.roomStatus);
		},
		events: function(location, config) {
			return NexudusUtil.getUpcomingEvents(config.nexudus, config.business, 3);
		},
		businesses: function(location, config) {
			return NexudusUtil.getBusinesses(config.nexudus, config.business, 6);
		}
	},
	request: function(nexudus, method, params) {
		return Bluebird.fromNode((cb) => {
			requestQueue.push({url: 'https://' + nexudus + '.spaces.nexudus.com/api/' + method, params: params})
				.on('finish', function (result) {
					cb(null, result);
				})
				.on('failed', function (err) {
					cb(err);
				});
		});
	},
	_requestLog: [],
	getRequestLog: function() {
		return NexudusUtil._requestLog;
	},
	getUpcomingEvents: function(nexudus, business, count, fromDate) {
		// Get a list of upcoming events
		count = count || 10;
		fromDate = fromDate || new Date();

		// Do request
		return NexudusUtil.request(nexudus, 'content/calendarevents', {
			orderby: 'StartDate',
			CalendarEvent_Business: business,
			from_CalendarEvent_StartDate: fromDate.toISOString(),
			dir: 'Ascending',
			size: count,
			page: 1
		})
		.then((result) => {
			if(!result.Records) {
				return [];
			}
			// Get a detailed list of each event
			return Bluebird.map(result.Records, function(event) {
				return NexudusUtil.request(nexudus, 'content/calendarevents/' + event.Id)
					.then((event) => {
						// Convert it to something nicer
						return {
							name: event.Name,
							description: event.ShortDescription,
							date: new Date(event.StartDate)
						}
					});
			});
		});
	},
	getRoomStatusById: function(nexudus, business, id) {
		// Get the detailed status of the room booking
		const now = new Date(); // 1470950101000 is a good timestamp for testing
		return NexudusUtil.request(nexudus, 'spaces/bookings', {
			Booking_Resource: id,
			from_Booking_ToTime: now.toISOString(),
			orderby: 'ToTime',
			dir: 'Ascending',
			page: 1
		})
		.then((result) => {
			// If we have a result, it's booked
			if(result && result.Records && result.Records.length) {
				const bookings = result.Records;
				let until;
				// Convert them to proper dates
				_.forEach(bookings, function(booking) {
					booking.FromTime = new Date(booking.FromTime);
					booking.ToTime = new Date(booking.ToTime);
				});

				// Is the room currently booked?
				const status = bookings[0].FromTime < now && bookings[0].ToTime > now;
				if(status) {
					// Currently booked. When do the consecutive bookings end?
					let lastEndTime = 0;
					// Loop over all bookings
					_.forEach(bookings, function(booking) {
						if(lastEndTime === 0) {
							// First loop is the start
							lastEndTime = booking.ToTime.getTime();
						} else if(booking.FromTime.getTime() < (lastEndTime + 960000)) {
							// Otherwise there's another booking.
							// Skip gaps smaller than 16mins between bookings (960000 is 16mins in ms)
							lastEndTime = booking.ToTime.getTime();
						} else {
							// No more bookings
							return false;
						}
					});
					// Convert the end of the bookings to a proper date
					until = new Date(lastEndTime);
				} else {
					// Not currently booked
					until = bookings[0].FromTime;
				}

				return {
					status: status ? 'booked' : 'open',
					until: until
				};
			} else {
				// No result. Room is free and clear!
				return {
					status: 'open',
					until: false
				};
			}
		})
		.catch(() => {
			return {
				status: 'booked',
				until: null
			};
		});
	},
	getRoomStatus: function(nexudus, business, enabledRooms) {
		// Get the status of all rooms with names in the "enabledRooms" array
		return NexudusUtil.request(nexudus, 'spaces/resources', {
			page: 1,
			size: 100,
			Resource_Business: business
		})
		.then((resources) => {
			// Find the rooms that match the array
			let rooms = resources.Records;
			if(enabledRooms && (enabledRooms.length > 0)) {
				rooms = _.filter(rooms, function(room) {
					return enabledRooms.indexOf(room.Name) >= 0;
				});
			}

			// Loop over each room and get it's detailed status
			let result = Bluebird.resolve({});
			_.forEach(rooms, function(room){
				result = result.then((resultObj) => {
					return NexudusUtil.getRoomStatusById(nexudus, business, room.Id)
					.then((statusResult) => {
						// Format the result as something nicer
						resultObj[room.Name] = {
							id: room.Id,
							name: room.Name.trim().replace(/^[A-Z]+ - /, ''), // TODO: is this ok? I guss so.
							status: statusResult.status,
							until: statusResult.until
						};
						return resultObj;
					});
				});
			});
			return result;
		});
	},
	hasProfilePic: function(nexudus, type, id) {
		// Do a head request just to check if the file is there
		return prequest({
			method: 'GET',
			resolveWithFullResponse: true,
			simple: false,
			url: 'https://' + nexudus + '.spaces.nexudus.com/en/'+type+'/getavatar/'+id+'?h=10&w=10'
		})
		.then((res) => {
			// A content length of 2949 would be the "default" image.
			// It's possible, though unlikely anyone else would have this exact filesize...
			return res.statusCode === 200
				&& res.headers['content-length'] !== '2949'
				&& res.headers['content-length'] !== '1229'
				&& !invalidDomains.test(res.req.path);
		});
	},
	getBusinesses: function(nexudus, business, count) {
		 // Look up teams, up to a limit of count * 4
		// for randomness, and to provide enough room for some entries to not have
		// profile pics
		const getTeams = (page) => {
			const currentPage = page || 1;
			return NexudusUtil.request(nexudus, 'spaces/teams', {
				page: currentPage,
				Team_Business: business,
				Team_ProfileIsPublic: true,
				size: 1000
			})
			.then((teamResult) => {
				const result = teamResult.Records || [];
				if(currentPage < teamResult.TotalPages) {
					return getTeams(currentPage + 1)
						.then((secondResult) => {
							return result.concat(secondResult);
						});
				}
				return result;
			});
		};

		return getTeams().then((teamResult) => {
			shuffleArray(teamResult);

			// Function for recursively checking if teams have pics
			// And stop when we've reached "count"
			const checkPic = function(obj, arr) {
				if(!obj) {
					return Bluebird.resolve(arr);
				}
				return NexudusUtil.hasProfilePic(nexudus, 'team', obj.Id)
					.then((hasPic) => {
						if(!hasPic) {
							return checkPic(teamResult.shift(), arr);
						}

						arr.push(obj);
						if(arr.length < count) {
							return checkPic(teamResult.shift(), arr);
						}
						return arr;
					})
					.catch(() => {
						return arr;
					});
			};

			// Check to make sure they have profile pics
			return checkPic(teamResult.shift(), [])
				.then((result) => {
					// Then format them a bit more nicely
					return _.map(result, function(team){
						return {
							name: team.Name,
							id: team.Id,
							social: {
								twitter: team.Twitter,
								facebook: team.Facebook,
								linkedin: team.LinkedIn
							}
						};
					});
				});
		})
	},
	getLocations: function(nexudus) {
		return NexudusUtil.request(nexudus, 'sys/businesses');
	}
};

module.exports = NexudusUtil;
