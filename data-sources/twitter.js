var prequest = require('request-promise'),
	Bluebird = require('bluebird'),
	_ = require('lodash');

var Twitter = require('twitter');

var isTweet = _.conforms({
	id_str: _.isString,
	text: _.isString,
	user: _.isObject
});

var TwitterUtil = {
	init: function(manager) {
		TwitterUtil.client = new Twitter({
			consumer_key: 'fXnXrxQXXF54s4PCbJIVODHPL',
			consumer_secret: '6SBX0BYch5EWd5iIwwsBVU9LrwL2PNhkcA6QBS3otjAsobrWrA',
			access_token_key: '17049363-EqPHmR0g5Wu7ILww6VigAssck4V8xnvDgosJurxqI',
			access_token_secret: 'kbFrpT85OhiKSqmLUNM0irVY8c98En6CYbQeeF6lweNou'
		});
		TwitterUtil.manager = manager;
	},
	timing: {
		tweet: 30
	},
	events: {
		tweet: function(location, config) {
			var hashtag = config.hashtag || null;
			
			if(!TwitterUtil.hashtags[location] || TwitterUtil.hashtags[location] != hashtag) {
				TwitterUtil.hashtags[location] = hashtag;
				if(TwitterUtil.stream[location]) {
					TwitterUtil.stream[location].destroy();
					delete TwitterUtil.stream[location];
					delete TwitterUtil.lastTweet[location];
					delete TwitterUtil.firstTweet[location];
				}
				if(hashtag) {
					TwitterUtil.initStream(location, hashtag);
				}
			}
			if(hashtag) {
				if(TwitterUtil.lastTweet[location]) {
					return TwitterUtil.lastTweet[location];
				}
				if(TwitterUtil.firstTweet[location]) {
					return TwitterUtil.firstTweet[location];
				}
				
				return TwitterUtil.firstTweet[location] = Bluebird.fromNode(function(cb) {
					setTimeout(function(){
						console.log('getTweet',location,hashtag);
						TwitterUtil.client.get('search/tweets', { q: config.hashtag || '#myCMPNY' }, cb);
					}, 10000 + Math.floor(Math.random() * 30000));
				})
				.then(function(tweets) {
					if(tweets && tweets.statuses && tweets.statuses.length) {
						return TwitterUtil.lastTweet[location] = TwitterUtil.normalizeTweet(tweets.statuses[0]);
					}
					
					return TwitterUtil.lastTweet[location] = {
						username: 'anybody',
						name: 'Anybody',
						picture: 'https://abs.twimg.com/sticky/default_profile_images/default_profile_0_normal.png',
						location: 'anywhere',
						date: new Date(),
						text: 'Nobody has tweeted yet!'
					};
				})
				.catch(function(err) {
					console.error(err);
					return TwitterUtil.lastTweet[location] = {
						username: 'anybody',
						name: 'Anybody',
						picture: 'https://abs.twimg.com/sticky/default_profile_images/default_profile_0_normal.png',
						location: 'anywhere',
						date: new Date(),
						text: 'Nobody has tweeted yet!'
					};
				});
			}
		}
	},
	firstTweet: {},
	lastTweet: {},
	stream: {},
	hashtags: {},
	initStream: function(location, hashtag) {
		setTimeout(function() {
			console.log('getStream',location,hashtag);
			TwitterUtil.stream[location] = TwitterUtil.client.stream('statuses/filter', { track: hashtag });
			TwitterUtil.stream[location].on('data', function(event) {
				if(isTweet(event) && !event.retweeted && !/^RT /.test(event.text)) {
					var tweet = TwitterUtil.normalizeTweet(event);
					TwitterUtil.lastTweet[location] = tweet;
					TwitterUtil.manager.emit.call(TwitterUtil.manager, 'twitter:live', location, tweet);
				}
			});
			
			TwitterUtil.stream[location].on('error', function(error) {
				console.error(error);
			});
			
			TwitterUtil.stream[location].on('end', function() {
				delete TwitterUtil.stream[location];
				delete TwitterUtil.hashtags[location];
			});
		}, 10000 + Math.floor(Math.random() * 30000))
	},
	normalizeTweet: function(tweet) {
		return {
			username: tweet.user && tweet.user.screen_name || 'anonymous',
			name: tweet.user && tweet.user.name || 'Anonymous',
			picture: tweet.user && tweet.user.profile_image_url_https || 'https://abs.twimg.com/sticky/default_profile_images/default_profile_0_normal.png',
			location: tweet.user && tweet.user.location,
			date: tweet.created_at && new Date(tweet.created_at) || new Date(),
			text: tweet.text
		};
	}
};

module.exports = TwitterUtil;