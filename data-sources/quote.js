var prequest = require('request-promise'),
	Bluebird = require('bluebird'),
	_ = require('lodash');

var QuoteUtil = {
	timing: {
		quote: 43200
	},
	events: {
		quote: function() {
			return QuoteUtil.getQuote();
		}
	},
	_quoteCache: Bluebird.resolve({
		quoteText: 'Any Quote API that you depend on will eventually start producing errors',
		quoteAuthor: 'Kevin Smith'
	}),
	_quoteKey: -1,
	getQuote: function() {
		// Date as of this morning
		var d = new Date(); d.setHours(0,0,0,0);
		
		// Get a key for the quote
		var quoteKey = ((d.getTime() / 1000) | 0) % 999999;
		if(QuoteUtil._quoteKey === quoteKey) {
			return QuoteUtil._quoteCache;
		}
		// console.log('Quote Key: ', quoteKey);
		QuoteUtil._quoteKey = quoteKey;
		
		// Get the quote, update the cache
		return QuoteUtil._quoteCache = prequest.get('http://api.forismatic.com/api/1.0/?method=getQuote&format=json&lang=en&key=' + quoteKey)
			.then(function(result) {
				// Decode json
				return JSON.parse(result);
			})
			.catch(function(){
				// Error! Reset the quote to error quote.
				QuoteUtil._quoteKey = -1;
				return {
					quoteText: 'Any Quote API that you depend on will eventually start producing errors',
					quoteAuthor: 'Kevin Smith'
				}
			});
	}
};

module.exports = QuoteUtil;