jQuery(function ($) {
	var getDevicePixelRatio = function() {
		var ratio = 1;
		// To account for zoom, change to use deviceXDPI instead of systemXDPI
		if (window.screen.systemXDPI !== undefined && window.screen.logicalXDPI       !== undefined && window.screen.systemXDPI > window.screen.logicalXDPI) {
			// Only allow for values > 1
			ratio = window.screen.systemXDPI / window.screen.logicalXDPI;
		}
		else if (window.devicePixelRatio !== undefined) {
			ratio = window.devicePixelRatio;
		}
		return ratio;
	};
	
	// Get a reference to the canvas object
	var canvas = document.getElementById('background');
	// Create an empty project and a view for the canvas:
	paper.setup(canvas);
	var pxR = getDevicePixelRatio();
	var paperWidth = canvas.width / pxR,
		paperHeight = canvas.height / pxR;
	var speed = paperHeight / 2048;

	var addTriangle = function() {
		if(triangles.length > 80) {
			return;
		}

		var size = Math.floor(30 * Math.random()) + 10;
		var triangle = new paper.Path.RegularPolygon([paperWidth / 2, (paperHeight / 2) + 1], 3, size);
		triangle.strokeColor = 'white';
		triangle.strokeWidth = 3;
		triangle.opacity = (Math.random() * 0.25) + 0.025;
		triangle.pivot = new paper.Point(triangle.position.x, triangle.position.y + (size * 0.24));
		triangle.position.x = Math.random() * paperWidth;
		triangle.position.y = paperHeight + size + 2;
		triangle.bg_life = (Math.floor(Math.random() * 10) + 4) * 2 * (1 / speed) * (50 / size);
		//triangle.bg_life = canvas.height * Math.random() * 10000 * speed;
		triangle.bg_speed = speed * (size / 50);
		

		triangles.push(triangle);
	};

	var triangles = [], triangleTime = 0;
	paper.view.onFrame = function(event) {
		$.each(triangles, function(index, triangle) {
			if(triangle) {
				triangle.bg_life -= event.delta;
				triangle.rotate(speed);
				triangle.position.y -= triangle.bg_speed;

				if(triangle.bg_life <= 0 && triangle.opacity > 0) {
					triangle.opacity = Math.max(0, triangle.opacity - 0.005);
				} else if(triangle.bg_life <= 0 && triangle.opacity <= 0) {
					triangle.remove();
					triangles.splice(index, 1); // Remove triangle
				} else if(triangle.position.y < -20) {
					triangle.remove();
					triangles.splice(index, 1); // Remove triangle
				}
			}
		});
		
		triangleTime += event.delta;
		if(triangleTime > (0.4 / speed)) {
			triangleTime = 0;
			addTriangle();
		}
	};
	addTriangle();
});