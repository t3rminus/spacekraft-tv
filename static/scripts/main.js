window.onerror = function(message, file, line, col, error) {
	var errDa = {
		message: ''+message,
		file: ''+file,
		line: ''+line,
		col: ''+col || '??',
		error: '??'
	};
	if(error && error.stack) {
		errDa.error = JSON.stringify(error.stack);
	}
	
	jQuery.ajax({
		method: 'POST',
		url: '/errlog',
		data: errDa,
		complete: function() {
			setTimeout(function(){
				window.location.reload();
			}, 5000);
		}
	});
};

function setCookie(name, value, expiry) {
	var d = new Date();
	d.setTime(d.getTime() + (expiry * 86400000)); // Day in seconds
	var expires = "expires="+ d.toUTCString();
	document.cookie = name + "=" + value + ";" + expires + ";path=/";
}

function getCookie(name) {
	var theName = name + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for(var i = 0; i <ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(theName) == 0) {
			return c.substring(theName.length, c.length);
		}
	}
	return "";
}

function toggleFullscreen(elem) {
	elem = elem || document.documentElement;
	if (!document.fullscreenElement && !document.mozFullScreenElement &&
		!document.webkitFullscreenElement && !document.msFullscreenElement) {
		if (elem.requestFullscreen) {
			elem.requestFullscreen();
		} else if (elem.msRequestFullscreen) {
			elem.msRequestFullscreen();
		} else if (elem.mozRequestFullScreen) {
			elem.mozRequestFullScreen();
		} else if (elem.webkitRequestFullscreen) {
			elem.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
		}
	} else {
		if (document.exitFullscreen) {
			document.exitFullscreen();
		} else if (document.msExitFullscreen) {
			document.msExitFullscreen();
		} else if (document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		} else if (document.webkitExitFullscreen) {
			document.webkitExitFullscreen();
		}
	}
}

jQuery(function ($) {
	var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
	
	var skycons = new Skycons({color: 'white'});
	
	var eventTemplateStr = '<li class="upcoming-events__item">' +
		'<div class="upcoming-events__date">{{dispDate}}<br />{{dispTime}}</div>' +
		'<div class="upcoming-events__content">' +
		'<h2>{{name}}</h2><p>{{description}}</p>' +
		'</div></li>';
	var roomTemplateStr = '<li class="room-status__item">' +
		'<div class="room-status__status status--{{status}}">{{{icon}}}</div>' +
		'<div class="room-status__info"><div class="room-status__name">{{name}}</div>' +
		'<div class="room-status__until">{{dispDate}}</div></div>' +
		'</li>';
	var memberTemplateStr = '<li class="our-members__item column">' +
		'<div class="our-members__image">' +
		'<div class="our-members__image-actual" style="background-image:url({{avatar}})"></div>' +
		'</div>' +
		'<div class="our-members__name">{{name}}</div>' +
		'</li>';
	var tweetTemplateStr = '<div class="tweet__contents">' +
		'<img src="{{picture}}" alt="@{{username}}" class="tweet__image" />' +
		'<div class="tweet__name">@{{username}} ({{name}})</div>' +
		'<div class="tweet__text">{{{text}}}</div>' +
		'<div class="tweet__meta">{{date}}{{#if location}}, {{location}}{{/if}}</div>' +
		'</div>';
	
	var eventTemplate = Handlebars.compile(eventTemplateStr);
	var roomTemplate = Handlebars.compile(roomTemplateStr);
	var memberTemplate = Handlebars.compile(memberTemplateStr);
	var tweetTemplate = Handlebars.compile(tweetTemplateStr);
	
	var youtube_parser = function(url){
		var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
		var match = url.match(regExp);
		return (match&&match[7].length==11)? match[7] : false;
	};
	
	var playVideo = function(player, vid) {
		if(/^(https?:|youtube\.com|youtu\.be)/.test(vid.url)) {
			vid.url = youtube_parser(vid.url);
		}
		player.loadVideoById({ videoId: vid.url, suggestedQuality: 'hd1080' });
		player.setVolume(vid.volume);
	};
	
	var $window = $(window),
		$body = $('body'),
		$clock = $('.js-time'),
		$dow = $('.js-dow'),
		youtubeCue = [],
		messageTimeout,
		clearModal,
		lastSite = '';
	
	var $weatherNow = $('.js-weather-now'),
		$weatherTonight = $('.js-weather-tonight'),
		$weatherTomorrow = $('.js-weather-tomorrow'),
		$weatherLater = $('.js-weather-later');
	
	var eventFunctions = {
		'weather': function(msg) {
			window.weather = msg;
			skycons.remove('skycon');
			skycons.remove('skycon-later');
			skycons.remove('skycon-tomorrow');
			skycons.remove('skycon-tonight');
			$weatherNow.html('<div class="dashboard__temperature--number">'+Math.round(msg.currently.temperature)+'°'+' now</div>');
			$weatherNow.prepend('<canvas id="skycon" class="skycon" width="256" height="256"></canvas>');
			skycons.add('skycon', msg.currently.icon);
			
			if(msg.hourly && msg.hourly.data) {
				var rollingWeather, tonight, tomorrow, later;
				var hr = (new Date()).getHours();
				if(hr < 18) {
					tonight = 19 - hr;
					if(msg.hourly.data[tonight]) {
						rollingWeather = msg.hourly.data[tonight];
						$weatherTonight.html('<div class="dashboard__temperature--number">'+Math.round(rollingWeather.temperature)+'°'+' tonight</div>');
						$weatherTonight.prepend('<canvas id="skycon-tonight" class="skycon skycon--inline"  width="64" height="64"></canvas>');
						skycons.add('skycon-tonight', rollingWeather.icon);
					}
					tomorrow = 33 - hr;
					if(msg.hourly.data[tomorrow]) {
						rollingWeather = msg.hourly.data[tomorrow];
						$weatherTomorrow.html('<div class="dashboard__temperature--number">'+Math.round(rollingWeather.temperature)+'°'+' tomorrow</div>');
						$weatherTomorrow.prepend('<canvas id="skycon-tomorrow" class="skycon skycon--inline"  width="64" height="64"></canvas>');
						skycons.add('skycon-tomorrow', rollingWeather.icon);
					}
					later = 43 - hr;
					if(msg.hourly.data[later]) {
						rollingWeather = msg.hourly.data[later];
						$weatherLater.html('<div class="dashboard__temperature--number">'+Math.round(rollingWeather.temperature)+'°'+' tomorrow night</div>');
						$weatherLater.prepend('<canvas id="skycon-later" class="skycon skycon--inline"  width="64" height="64"></canvas>');
						skycons.add('skycon-later', rollingWeather.icon);
					}
				} else {
					// Actually tomorrow
					tonight = 34 - hr;
					if(msg.hourly.data[tonight]) {
						rollingWeather = msg.hourly.data[tonight];
						$weatherTonight.html('<div class="dashboard__temperature--number">'+Math.round(rollingWeather.temperature)+'°'+' tomorrow</div>');
						$weatherTonight.prepend('<canvas id="skycon-tonight" class="skycon skycon--inline"  width="64" height="64"></canvas>');
						skycons.add('skycon-tonight', rollingWeather.icon);
					}
					// Tomorrow night
					tomorrow = 43 - hr;
					if(msg.hourly.data[tomorrow]) {
						rollingWeather = msg.hourly.data[tomorrow];
						$weatherTomorrow.html('<div class="dashboard__temperature--number">'+Math.round(rollingWeather.temperature)+'°'+' tomorrow night</div>');
						$weatherTomorrow.prepend('<canvas id="skycon-tomorrow" class="skycon skycon--inline" width="64" height="64"></canvas>');
						skycons.add('skycon-tomorrow', rollingWeather.icon);
					}
					if(msg.daily && msg.daily.data) {
						// The next day
						if(msg.hourly.data[1]) {
							rollingWeather = msg.hourly.data[1];
							$weatherLater.html('<div class="dashboard__temperature--number">'+Math.round(rollingWeather.temperature)+'°'+' ' +  days[(new Date()).getDay() + 2] + '</div>');
							$weatherLater.prepend('<canvas id="skycon-later" class="skycon skycon--inline"  width="64" height="64"></canvas>');
							skycons.add('skycon-later', rollingWeather.icon);
						}
					}
				}
			}
			skycons.play();
		},
		'quote': function(msg) {
			$('.js-quote-text').text(msg.quoteText);
			$('.js-quote-author').text('—' + (msg.quoteAuthor || 'Anonymous'));
		},
		'website': function(data) {
			if(clearModal) {
				clearModal();
				if(!data.url) {
					return;
				}
			}
			var url = data.url;
			if(!/^https?:/.test(url)) {
				url = 'http://' + url;
			}
			
			var $website = $('#website');
			if($website.length) {
				if(url == lastSite) {
					lastSite = '';
					$website.remove();
				} else {
					lastSite = url;
					$website.prop('src', url);
				}
			} else {
				lastSite = url;
				$website = $('<iframe id="website" frameborder="0" />');
				$website.prop('src', url);
				$website.appendTo($body);
			}
			clearModal = function(){
				lastSite = '';
				$website.remove();
				clearModal = null;
			};
		},
		'youtube': function(data) {
			var $youtube = $('#youtube');
			
			if(data.stop == '1') {
				if(clearModal) {
					clearModal();
				}
				return;
			}
			
			modal = true;
			if($youtube.length) {
				if(data.queue == 'queue') {
					youtubeCue.push(data);
				} else if(data.queue == 'now') {
					playVideo($youtube.data('player'), data);
				} else if(data.queue == 'next') {
					youtubeCue.unshift(data);
				} else if(data.queue == 'clear_now') {
					youtubeCue = [];
					playVideo($youtube.data('player'), data);
				} else if(data.queue == 'clear_next') {
					youtubeCue = [];
					youtubeCue.push(data);
				}
				return;
			} else {
				if(clearModal) {
					clearModal();
				}
				
				$youtube = $('<div id="youtube"><div id="ytplayer"></div></div>').appendTo($body);
			}
			var player = new YT.Player('ytplayer', {
				height: '1920',
				width: '1080',
				playerVars: {
					controls: 0,
					modestbranding: 1,
					rel: 0,
					fs: 0
				},
				events: {
					'onReady': function(event) {
						$youtube.data('player', event.target);
						playVideo(event.target, data);
					},
					'onStateChange': function(event) {
						if(event.data == YT.PlayerState.ENDED) {
							if(youtubeCue.length) {
								playVideo(player, youtubeCue.shift());
							} else {
								if(clearModal) {
									clearModal();
								}
							}
						}
					}
				}
			});
			clearModal = function() {
				youtubeCue = [];
				$youtube.remove();
				$body.focus();
				clearModal = null;
			};
		},
		'message': function(data) {
			var $orgModal = $('.alert');
			
			if(data.chime == '1') {
				$('#chime')[0].play();
			}
			
			if($orgModal.length) {
				$orgModal.fadeOut(200, function(){
					$orgModal.remove();
				});
				clearModal = null;
			}
			
			var $modal = $('<div class="alert"></div>');
			$modal.append('<h1 class="alert__title">'+data.title+'</h1>');
			$modal.append('<div class="alert__body">'+data.body+'</div>');
			$body.append($modal);
			$modal.css('display','block');
			var height = $modal.outerHeight();
			$modal.css({
				display: 'none',
				top: ($body.outerHeight() - height) / 2
			})
			.fadeIn(200);
			
			var $frames = $('.frames');
			if($frames.is(':visible')) {
				$frames.fadeOut(200);
			}
			
			clearModal = function() {
				$modal.fadeOut(200, function(){
					$modal.remove();
				});
				$frames.fadeIn(200);
				clearModal = null;
			};
			
			if(messageTimeout) {
				clearTimeout(messageTimeout);
			}
			messageTimeout = setTimeout(clearModal, 30000);
		},
		'refresh': function() {
			window.location.reload();
		},
		'spacekraft.events': function(events) {
			var $upcoming = $('.js-upcoming').empty();
			$.each(events, function(){
				var date = new Date(this.date);
				this.dispDate = moment(date).format('MMM Do');
				this.dispTime = moment(date).format('h:mma');
				$upcoming.append(eventTemplate(this));
			});
		},
		'spacekraft.rooms': function(rooms) {
			var $roomstatus = $('.js-roomstatus').empty();
			var tmpRooms = [];
			$.each(rooms, function() {
				tmpRooms.push(this);
			});
			rooms = tmpRooms;
			if(rooms.length > 4) {
				if(rooms.length > 6) {
					rooms = rooms.slice(0,6);
				}
				$('.js-quote-text').closest('.column').css({ display: 'none' });
				$roomstatus.removeClass('small-up-2').addClass('small-up-3');
				$roomstatus.closest('.column').removeClass('small-8').addClass('small-12');
			} else {
				$('.js-quote-text').closest('.column').css({ display: 'block' });
				$roomstatus.removeClass('small-up-3').addClass('small-up-2');
				$roomstatus.closest('.column').removeClass('small-12').addClass('small-8');
			}
			
			$.each(rooms, function(){
				if(this.until) {
					var date = moment(this.until);
					if(date > moment().endOf('week')) {
						this.dispDate = this.status + ' until next week';
					} else if(date > moment().endOf('day')) {
						this.dispDate = this.status + ' for the day';
					} else {
						this.dispDate = this.status + ' for ' + date.toNow(true);
					}
				} else if(this.until === false) {
					this.dispDate = this.status + ' until next week';
				} else {
					this.status = 'error';
					this.dispDate = 'Information unavailable';
				}
				
				if(this.status == 'open') {
					this.icon = '<i class="fa fa-circle"></i>';
				} else {
					this.icon = '<i class="fa fa-times-circle"></i>';
				}
				
				$roomstatus.append(roomTemplate(this));
			});
		},
		'spacekraft.businesses': function(members) {
			var $ourmembers = $('.js-ourmembers').empty();
			$.each(members, function(){
				this.avatar = 'https://' + nexudus + '.spaces.nexudus.com/en/team/getavatar/' + this.id + '?h=256&w=400&mode=max';
				var $me = $(memberTemplate(this));
				$ourmembers.append($me);
			});
		},
		'tweet': function(tweet) {
			if(tweet && tweet.text) {
				tweet.picture = tweet.picture.replace(/_normal\./, '_bigger.');
				tweet.date = moment(tweet.date).fromNow();
				var $tweet = $('.js-tweet');
				var $me = $(tweetTemplate(tweet));
				$tweet.empty().append($me);
			}
		},
		'livetweet': function(tweet) {
			var $orgModal = $('.alert');
			
			if($orgModal.length) {
				$orgModal.fadeOut(200, function(){
					$orgModal.remove();
				});
				clearModal = null;
			}
			
			var $modal = $('<div class="alert"></div>');
			
			tweet.picture = tweet.picture.replace(/_normal\./, '_bigger.');
			tweet.date = moment(tweet.date).fromNow();
			var $me = $(tweetTemplate(tweet));
			$modal.append($me);
			
			$body.append($modal);
			$modal.css('display','block');
			var height = $modal.outerHeight();
			$modal.css({
				display: 'none',
				top: ($body.outerHeight() - height) / 2
			})
			.fadeIn(200);
			
			var $frames = $('.frames');
			if($frames.is(':visible')) {
				$frames.fadeOut(200);
			}
			
			clearModal = function() {
				$modal.fadeOut(200, function(){
					$modal.remove();
				});
				$frames.fadeIn(200);
				clearModal = null;
			};
			
			if(messageTimeout) {
				clearTimeout(messageTimeout);
			}
			messageTimeout = setTimeout(clearModal, 10000);
		}
	};
	
	var socket = io(window.location.origin);
	
	socket.on('connect', function() {
		socket.emit('init', {
			location: theLocation,
			display: true
		});
		
		$.each(eventFunctions, function(event, handler) {
			socket.off(event);
			socket.on(event, handler);
		});
	});

	setInterval(function(){
		var today = new Date();
		var h = (today.getHours() % 12) + '';
		var m = today.getMinutes() + '';
		// var s = today.getSeconds() + '';
		
		if (h == '0') {
			h = '12';
		}
		m = m.length == 1 ? '0' + m : m;
		// s = s.length == 1 ? '0' + s : s;
		$clock.html(h + ':' + m);
		
		// Refresh several times a day
		var interval = 14400000; // 4h in ms
		var lastRefresh = parseInt(getCookie('lastRefresh')) || 0;
		if((today.getTime() - lastRefresh) > interval) {
			setCookie('lastRefresh', today.getTime());
			window.location.reload(true);
		}
		
		$dow.html('Happy ' + days[today.getDay()] + '!');
	}, 1000);
	
	var $carousel = $('.frames'),
		$frames = $carousel.children('.frame'),
		$currentFrame = $frames.filter(':eq(0)'),
		$firstFrame = $currentFrame,
		firstFrameDelay = 2,
		animationSpeed = 2000;
	
	var nextFrame = function(){
		if(clearModal) {
			return;
		}
		
		if($currentFrame.get(0) === $firstFrame.get(0) && firstFrameDelay > 0) {
			firstFrameDelay--;
			return;
		}
		
		var $nextFrame = $currentFrame.next();
		if(!$nextFrame.length) {
			$nextFrame = $firstFrame;
			firstFrameDelay = 2;
		}
		if(animationSpeed === 0) {
			$frames.hide().css('opacity',1);
			$nextFrame.show();
		} else {
			$currentFrame.animate({ opacity: 0 }, animationSpeed);
			$nextFrame.animate({ opacity: 1 }, animationSpeed);
		}
		
		$currentFrame = $nextFrame;
	};
	
	if(window.location.hash === '#dev') {
		animationSpeed = 0;
		firstFrameDelay = 0;
		$('body').on('click', nextFrame);
	} else {
		if($frames.length > 1) {
			setInterval(nextFrame, 15000);
		}
	}
	
});