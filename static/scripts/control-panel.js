jQuery(function ($) {
	var $body = $('body');
	
	$('form .controls').hide();
	$body.on('click focus', '.option-box, .option-box *', function(e){
		var $optionBox = $(this).closest('.option-box');
		$('.selected').removeClass('selected');
		$optionBox.addClass('selected');
		var $formControls = $optionBox.find('form .controls');
		$('.footer-controls')
			.empty().append($formControls.html())
			.find('button').each(function(idx){
				$(this).on('click', function(e) {
					e.preventDefault();
					$formControls.find('button:eq('+idx+')').trigger('click');
				});
			});
		$('footer').slideDown();
	});
	$body.on('click', function(e) {
		if(!$(e.target).is('.option-box, .option-box *')) {
			$('footer').slideUp();
			$('.selected').removeClass('selected');
		}
	});
	
	$body.on('click', '.js-imgAdd, .js-imgRemove', function(e){
		e.preventDefault();
		if($(this).hasClass('js-imgAdd')) {
			$('.js-imageSlides').append('<div class="row column"><input type="text" name="imageSlides[]" placeholder="Image URL" /></div>');
		} else {
			$('.js-imageSlides').find(':last').remove();
		}
	});
	
	$body.on('click', '.js-roomAdd, .js-roomRemove', function(e){
		e.preventDefault();
		if($(this).hasClass('js-roomAdd')) {
			$('.js-roomStatus').append('<div class="row column"><input type="text" name="roomStatus[]" placeholder="Room Name" /></div>');
		} else {
			$('.js-roomStatus').find(':last').remove();
		}
	});
	
	$body.on('click', 'form.ajax button[type=submit][name]', function() {
		$(this).addClass('clicked');
	});
	
	$body.on('submit','form.ajax', function(e) {
		e.preventDefault();
		var $form = $(this);
		var dataArray = $form.serializeArray();
		
		$form.find('button.clicked').each(function(){
			dataArray.push({
				name: $(this).attr('name'),
				value: $(this).val()
			});
		});
		
		var url = $form.prop('action') || window.location.protocol + '//' + window.location.host + window.location.pathname;
		$.ajax({
			method: 'POST',
			url: url,
			data: dataArray,
			dataType: 'JSON',
			success: function(){
				if(!$form.hasClass('preserve')) {
					$form[0].reset();
				}
			},
			error: function() {
				alert('Error');
			},
			complete: function() {
				$('button.clicked').removeClass('clicked');
			}
		})
	});
});