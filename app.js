'use strict';

var express = require('express'),
	path = require('path'),
	_ = require('lodash'),
	Bluebird = require('bluebird'),
	app = require('./lib/server'),
	Manager = require('./lib/manager'),
	QuickDb = require('./lib/quickdb');

/**
 * Initializes data sources
 */
var nexudusSource = require('./data-sources/nexudus'),
	quoteSource = require('./data-sources/quote'),
	weatherSource = require('./data-sources/weather'),
	twitterSource = require('./data-sources/twitter');

/**
 * The Database
 */

var dataDirectory = path.join(__dirname,'content');
var db = QuickDb(dataDirectory);

var errLog = [];
app.post('/errlog', function(req, res) {
	errLog.push({
		date: new Date(),
		message: req.body.message,
		file: req.body.file + ' (' + req.body.line + ':' + req.body.col + ')',
		error: req.body.error
	});
	if(errLog.length > 100) {
		errLog = errLog.slice(errLog.length - 100);
	}
	res.status(200);
	res.end();
});

app.get('/:location', function (req, res, next) {
	var location = req.params.location;
	db.get(location)
		.then(function(content) {
			content = _.cloneDeep(content) || {};
			content.location = location;
			
			if(content.social) {
				var newSocial = {};
				_.forEach(content.social, function(value, network) {
					var tempValue = { network: network, link: value };
					switch(network) {
						case 'instagram':
							tempValue.icon = 'fa-instagram';
							break;
						default:
							tempValue.icon = 'fa-'+network+'-square';
							break;
					}
					newSocial[network] = tempValue;
				});
				content.social = newSocial;
			}
			
			res.render('index', content);
		})
		.catch(function() {
			// Any problems just do something else
			next('route');
		});
});

var protect = function(urlBase) {
	return function(req, res, next) {
		if (req.isAuthenticated()) {
			return next();
		}
		var location = req.params.location || 'burnaby';
		res.redirect(urlBase + '/' + location + '/login');
	};
};

app.get('/control-panel/:location/login', function(req, res) {
	var location = req.params.location;
	db.get(location)
		.then(function(content){
			content.messages = req.flash('error');
			res.render('login', content);
		});
});

app.post('/control-panel/:location/login', function(req, res, next) {
	var location = req.params.location;
	
	app._passport.authenticate('local', {
		successRedirect: '/control-panel/' + location,
		failureRedirect: '/control-panel/' + location + '/login',
		failureFlash: true
	})(req, res, next);
});

app.get('/control-panel/:location/logout', function(req, res, next) {
	var location = req.params.location;
	req.logout();
	res.redirect('/control-panel/'+location+'/login');
});

app.get('/control-panel/:location',
	protect('/control-panel'),
	function(req, res) {
		var location = req.params.location;
		db.get(location)
			.then(function(content) {
				content = _.clone(content);
				if(!content.business || req.query.changeBusiness) {
					return nexudusSource.getLocations(content.nexudus)
					.then(function(result) {
						if(result && result.Records && result.Records.length) {
							content.nexudusLocations = result.Records.map(function(loc){
								if(+loc.Id === +content.business) {
									loc.selected = true;
								}
								return loc;
							});
						} else {
							content.nexudusLocations = [];
						}
						return content;
					})
					.catch(function() {
						// Nexudus can fail. Just ignore it for now
						content.nexudusLocations = false;
						return content;
					});
				}
				return content;
			})
			.then(function(content) {
				content = _.cloneDeep(content);
				content.location = location;
				content.username = req.user;
				res.render('control-panel', content);
			});
	}
);

app.post('/control-panel/:location',
	protect('/control-panel'),
	function(req, res) {
		var location = req.params.location;
		var event = req.body.event;
		var data = _.cloneDeep(req.body);
		delete data.event;
		delete data.location;
		delete data.nexudusLocations;
		delete data._locals;
		delete data.messages;
		
		Bluebird.try(function() {
			if (data.refresh === '1') {
				if (location) {
					io.sockets.in('display-' + location).emit('refresh');
				} else {
					io.sockets.emit('refresh');
				}
			} else if (data.save === '1' && location) {
				delete data.save;
				if(data._key) {
					if(!data[data._key]) {
						data[data._key] = null;
					}
					delete data._key;
				}
				return db.set(location, data)
					.then(function () {
						io.sockets.in('display-' + location).emit('refresh');
					});
			} else {
				if (location) {
					io.sockets.in('display-' + location).emit(event, data);
				} else {
					io.sockets.emit(event, data);
				}
			}
		})
		.then(function(){
			if(req.headers['x-requested-with'] === 'XMLHttpRequest') {
				res.end(JSON.stringify({ success: true }));
			} else {
				res.redirect('/control-panel/'+location);
			}
		});
	}
);

app.use(express.static(__dirname + '/static'));

var listener = app.listen(app.get('port'), function() {
	console.log("Listening on " + listener.address().port);
});

/**
 * Main application manager
 */
var manager = new Manager(db);

/**
 * Socket Stuff
 */
var io = require('socket.io').listen(listener);

io.on('connection', function(socket) {
	socket.on('init', function(data) {
		if(data.location) {
			socket.join(data.location);
			if(data.display) {
				socket.join('display-'+data.location);
				var tmpData;
				if(tmpData = manager.getData('nexudus','bookings',data.location)) {
					socket.emit('spacekraft.rooms', tmpData);
				}
				if(tmpData = manager.getData('nexudus','events',data.location)) {
					socket.emit('spacekraft.events', tmpData);
				}
				if(tmpData = manager.getData('nexudus','businesses',data.location)) {
					socket.emit('spacekraft.businesses', tmpData);
				}
				if(tmpData = manager.getData('quote','quote',data.location)) {
					socket.emit('quote', tmpData);
				}
				if(tmpData = manager.getData('weather','weather',data.location)) {
					socket.emit('weather', tmpData);
				}
				if(tmpData = manager.getData('twitter','tweet',data.location)) {
					socket.emit('tweet', tmpData);
				}
			}
		}
	});
});

db.stores()
.then(function(stores){
	stores.forEach(function(store) {
		db.on(store + ':weather', function() {
			manager.triggerUpdate('weather', 'weather');
		});
		db.on(store + ':roomStatus', function() {
			manager.triggerUpdate('nexudus', 'bookings');
		});
	});
});

manager.on('ready', function() {
	// Add sources
	manager.addSource('nexudus', nexudusSource);
	manager.addSource('quote', quoteSource);
	manager.addSource('weather', weatherSource);
	manager.addSource('twitter', twitterSource);
	
	// Nexudus
	manager.on('nexudus:bookings', function(location, data) {
		io.to('display-'+location).emit('spacekraft.rooms', data);
	});
	manager.on('nexudus:events', function(location, data) {
		io.to('display-'+location).emit('spacekraft.events', data);
	});
	manager.on('nexudus:businesses', function(location, data) {
		io.to('display-'+location).emit('spacekraft.businesses', data);
	});
	
	// Quote
	manager.on('quote:quote', function(location, data) {
		io.to('display-'+location).emit('quote', data);
	});
	
	// Weather
	manager.on('weather:weather', function(location, data) {
		io.to('display-'+location).emit('weather', data);
	});
	
	// Twitter
	manager.on('twitter:tweet', function(location, data) {
		io.to('display-'+location).emit('tweet', data);
	});
	manager.on('twitter:live', function(location, data) {
		io.to('display-'+location).emit('livetweet', data);
	});
});
