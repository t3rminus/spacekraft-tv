FROM mhart/alpine-node:9.4.0

VOLUME /src/content

WORKDIR /src
ADD . .

# If you have native dependencies, you'll need extra tools
# RUN apk add --no-cache make gcc g++ python

# If you need npm, don't use a base tag
RUN npm install

EXPOSE 5000
CMD ["node", "app.js"]