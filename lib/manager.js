var EventEmitter = require('events').EventEmitter,
	Bluebird = require('bluebird'),
	util = require('util'),
	fs = Bluebird.promisifyAll(require('fs'));

if(!Object.prototype.hopEach) {
	Object.defineProperty(Object.prototype, 'hopEach', {
		writable: true,
		configurable: true,
		enumerable: false,
		value: function(cb) {
			var _this = this;
			Object.keys(_this).forEach(function(key) {
				if(!_this.hasOwnProperty(key)) { return; }
				cb.apply(_this, arguments);
			});
		}
	});
}

var isFunction = function(obj) {
	return !!(obj && obj.constructor && obj.call && obj.apply);
};


var Manager = function(db) {
	if (!(this instanceof Manager)) {
		return new Manager(db);
	}
	
	var _this = this;
	_this.callbacks = {};
	_this.data = {};
	_this.sources = {};
	_this.stores = [];
	_this.db = db;
	
	_this.config = {};
	
	db.stores()
		.then(function(allStores){
			_this.stores = allStores;
			
			_this.emit('ready');
			
			_this.interval = setInterval(function(){
				_this.tick();
			}, 1000);
		});
};

util.inherits(Manager, EventEmitter);
module.exports = Manager;

Manager.prototype.addIntervalEvent = function Manager_addIntervalEvent(callback, ticks, args, thisArg, sourceNamespace, variance) {
	var _this = this;
	args = args || [];
	sourceNamespace = sourceNamespace || '__no_sourceNamespace__';
	_this.callbacks[sourceNamespace] = _this.callbacks[sourceNamespace] || [];
	
	var cb;
	if(variance) {
		cb = function() {
			setTimeout(function(){
				callback.apply(thisArg, args);
			}, Math.floor(Math.random() * variance));
		};
	} else {
		cb = function() {
			callback.apply(thisArg, args);
		};
	}
	
	_this.callbacks[sourceNamespace].push({
		current: 0,
		interval: ticks,
		callback: cb
	});
};

Manager.prototype.tick = function Manager_tick() {
	var _this = this;
	_this.callbacks.hopEach(function(nsp){
		_this.callbacks[nsp].forEach(function(item){
			if(item.current % item.interval === 0) {
				item.current = 0;
				item.callback();
			}
			item.current++;
		});
	});
	_this.emit('tick');
};

Manager.prototype.addSource = function Manager_addSource(sourceName, source) {
	var _this = this;
	_this.sources[sourceName] = source;
	
	if(source.init) {
		source.init(this);
	}
	
	if(source.events && source.timing) {
		source.events.hopEach(function(eventName) {
			if(!source.timing[eventName]) {
				return;
			}
			
			var callback = function() {
				_this.stores.forEach(function(configName) {
					_this.db.get(configName)
						.then(function(configData) {
							Bluebird.resolve(source.events[eventName].call(null, configName, configData))
								.then(function(resultData) {
									_this.data[configName] = _this.data[configName] || {};
									_this.data[configName][sourceName] = _this.data[configName][sourceName] || {};
									_this.data[configName][sourceName][eventName] = _this.data[configName][sourceName][eventName] || {};
									_this.data[configName][sourceName][eventName] = resultData;
									
									_this.emit(sourceName + ':' + eventName, configName, _this.data[configName][sourceName][eventName]);
									_this.emit(sourceName, configName, _this.data[configName][sourceName]);
								});
						})
				});
			};
			
			_this.addIntervalEvent(callback, source.timing[eventName], null, source, sourceName + ':' + eventName, Math.floor(Math.random() * 10000));
		});
	}
};

Manager.prototype.getWatchData = function Manager_watchData(sourceName, eventName, callback) {
	var _this = this;
	if(isFunction(eventName)) {
		callback = eventName;
		eventName = null;
	}
	
	_this.on(eventName ? sourceName + ':' + eventName : sourceName, callback);
	_this.stores.forEach(function(configName) {
		if(eventName && _this.data[configName] && _this.data[configName][sourceName] && _this.data[configName][sourceName][eventName]) {
			callback(configName, _this.data[configName][sourceName][eventName]);
		} else if(_this.data[configName] && _this.data[configName][sourceName]) {
			callback(configName, _this.data[configName][sourceName]);
		}
	});
};

Manager.prototype.getData = function Manager_getData(sourceName, eventName, configName) {
	var _this = this;
	if(eventName && _this.data[configName] && _this.data[configName][sourceName] && _this.data[configName][sourceName][eventName]) {
		return _this.data[configName][sourceName][eventName];
	} else if(_this.data[configName] && _this.data[configName][sourceName]) {
		return _this.data[configName][sourceName];
	}
};

Manager.prototype.triggerUpdate = function Manager_triggerUpdate(sourceName, eventName) {
	if(Array.isArray(this.callbacks[sourceName + ':' + eventName])) {
		this.callbacks[sourceName + ':' + eventName].forEach(function(eventInfo){
			eventInfo.callback();
		})
	}
};