var EventEmitter = require('events').EventEmitter,
	Bluebird = require('bluebird'),
	util = require('util'),
	fs = Bluebird.promisifyAll(require('fs')),
	path = require('path'),
	_ = require('lodash');

var QuickDb = function(storePath) {
	if (!(this instanceof QuickDb)) {
		return new QuickDb(storePath);
	}
	this.inMemory = {};
	this.storePath = storePath;
};

util.inherits(QuickDb, EventEmitter);
module.exports = QuickDb;

QuickDb.prototype._loadStore = function QuickDb__loadStore(store) {
	if(!this.inMemory[store]) {
		this.inMemory[store] =
			fs.readFileAsync(path.join(this.storePath, store + '.json'), { encoding: 'utf8' })
				.then(function(result) {
					return JSON.parse(result);
				})
				.catch(function(){
					throw new Error('Unable to read store: ' + store);
				});
	}
	return this.inMemory[store];
};

QuickDb.prototype.stores = function QuickDb_stores() {
	return fs.readdirAsync(this.storePath)
		.then(function(results){
			return results.map(function(result) {
				return result.replace(/\.json$/i,'');
			});
		});
};

QuickDb.prototype.get = function QuickDb_get(store, key) {
	return this._loadStore(store)
		.then(function(storeVal) {
			if(storeVal && key) {
				return storeVal[key];
			} else if(!key) {
				return storeVal;
			}
		});
};

QuickDb.prototype.set = function QuickDb_get(store, key, value) {
	var _this = this;
	return _this._loadStore(store)
		.then(function(storeVal) {
			var newStore = _.clone(storeVal);
			if(value) {
				newStore[key] = value;
			} else {
				_.assign(newStore, key);
			}
			// Attempt to save the file
			return fs.writeFileAsync(path.join(_this.storePath, store + '.json'), JSON.stringify(newStore, null, 4))
				.then(function() {
					// Done. Emit events. Update in-memory store
					if(value) {
						_this.emit(store + ':' + key, value);
					} else {
						_.forEach(key, function(value, key) {
							_this.emit(store + ':' + key, value);
						});
					}
					_this.inMemory[store] = Bluebird.resolve(newStore);
				});
		});
};