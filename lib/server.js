'use strict';

var express = require('express'),
	fs = require('fs'),
	path = require('path'),
	util = require('util'),
	_ = require('lodash'),
	exphbs  = require('express-handlebars'),
	config = require('../config.json'),
	passport = require('passport'),
	LocalStrategy = require('passport-local').Strategy,
	BodyParser = require('body-parser'),
	CookieParser = require('cookie-parser'),
	CookieSession = require('cookie-session'),
	ConnectFlash = require('connect-flash'),
	app = express();

require('http').createServer(app);
app.set('port', (process.env.PORT || 5000));

var hbs = exphbs.create();
var helpers = require('handlebars-helpers')({ handlebars: hbs.handlebars });

// Register `hbs.engine` with the Express app.
app.engine('handlebars', hbs.engine);
app.set('view engine', 'handlebars');


// Define logins as environment variables to keep them out of repo
var logins = process.env.LOGINS.split(',');
var validUsers = {};
_.forEach(logins, function(login) {
	var theLogin = login.split(':');
	if(theLogin.length == 2) {
		validUsers[theLogin[0]] = theLogin[1];
	}
});

passport.use(new LocalStrategy(
	function(username, password, done) {
		username = (username && username.toLowerCase()) || '';
		
		if(password === validUsers[username]) {
			done(null, username);
		} else if(validUsers[username]) {
			done(null, false, { message: 'Incorrect password' });
		} else {
			done(null, false, { message: 'Unknown username' });
		}
	}
));

passport.serializeUser(function(user, done) { done(null, user); });
passport.deserializeUser(function(user, done) { done(null, user); });

app.use(CookieParser());
app.use(BodyParser.urlencoded({ extended: true }));
app.use(BodyParser.json());
app.use(CookieSession({ secret: 'keyboard cat' }));
app.use(ConnectFlash());
app.use(passport.initialize());
app.use(passport.session());

app._passport = passport;

module.exports = app;